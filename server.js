var net = require("net");

var server_config = {
  port: 10000,
  host: "0.0.0.0"
};
var server = net.createServer(socket => {});
const PEERS = process.env.PEERS.split(",");
const ID = process.env.ID;
var current = 2;

function ping_peers() {
  PEERS.map(peer => {
    /*  var client = new net.Socket(peer_config, () => {});
    client.connect(peer_config, () => { */
    const client = net.createConnection(10000, peer, () => {
      client.write(
        JSON.stringify({
          command: "ping",
          data: {}
        })
      );
    });

    client.on("data", data => {
      console.log(`<client from ${peer}> Received: ${data}`);
    });

    client.on("close", () => {
      client.destroy(); // kill client after server's response
      console.log("Connection closed");
    });
  });
}

setInterval(function() {
  current = (current + 1) % 3;

  if (ID == current) {
    console.log(`\n\n\n --- start ping on node${ID} for this round---`);
    ping_peers();
  }
}, 5000);

server.listen(server_config);

server.on("listening", sock => {
  console.log("Server listening\r");
});

server.on("connection", sock => {
  /* [remoteAddr, remotePort] = [sock.remoteAddress, sock.remotePort];
  [localAddr, localPort] = [sock.localAddress, sock.localPort]; */

  sock.on("data", data => {
    data = JSON.parse(data);

    if (data.command === "ping") {
      console.log(`<server> Received ${data.command}`);
      const message = `Pong!`;

      sock.write(message);
    } else {
      console.log("No command found");
    }
  });
});
