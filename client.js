var net = require('net');

var client = new net.Socket();
var server_config = {
    port: 10000,
    host: '127.0.0.1'
}
client.connect(server_config, () => {
	console.log('Connected');
	client.write(JSON.stringify({
        'command': 'ping',
        'data': {}
    }));
    /* console.log('data!', {
        'command': 'ping',
        'data': {}
    }) */
});

client.on('data', (data) => {
    console.log('<client> Received: ' + data);
});

client.on('close', () => {
    client.destroy(); // kill client after server's response
    console.log('Connection closed');
});
